import {cardHtmlDispatcher} from './card-templates.js'

const userAccount = document.getElementById("userAccount");
const userCards = document.getElementById("userCards");
const userScore = document.getElementById("userScore");
const userBJ = document.getElementById("userBJ");

const bankCards = document.getElementById("bankCards");
const bankScore = document.getElementById("bankScore");
const bankBJ = document.getElementById("bankBJ");

const resetButton = document.getElementById('reset')
const drawButton = document.getElementById('drawer')
const skipButton = document.getElementById('skip')

const incBet = document.getElementById('increaseBet')
const decBet = document.getElementById('decreaseBet')

const gameState = document.getElementById('gameState')
const gameBet = document.getElementById('gameBet')

skipButton.addEventListener('click', () => skip())
drawButton.addEventListener('click', () => drawCard())
resetButton.addEventListener('click', () => init())
document.getElementById('refillAccount').addEventListener('click', () => setUserAccount(user.account + 150))

incBet.addEventListener('click', () => setBet(game.bet+1))

decBet.addEventListener('click', () =>  {
    setBet(game.bet-1)
})


const WIN_VALUE = 21;
const AVAILABLE_BETS = [5,10,20,50,100,200,500]

// User
const user = {
    cards: [],
    score: 0,
    account: 100,
    bj: false,
};

// Bank
const bank = {
    cards: [],
    score: 0,
    bj: false

};

// Game

const game = {
    cards: [],
    bet: 0,
    betTaken: false
}

// Card Game
const cardGame = [];
const heads = [
    {key: "J", value: 10},
    {key: "Q", value: 10},
    {key: "K", value: 10},
    {key: "A", value: 11}
];

const numbers = [
    ...Array.from({length: 9}, (_, i) => {
        return {key: i + 2, value: i + 2};
    }),
    ...heads
];

const colors = [
    {name: "spades", color: "#000", symbol: "♠"},
    {name: "clubs", color: "#000", symbol: "♣"},
    {name: "diamonds", color: "#F00", symbol: "♦"},
    {name: "hearts", color: "#F00", symbol: "♥"}
];

function setUserAccount(value = 0) {
    user.account = value
    userAccount.innerHTML = `${value}€`;
}

function addToUserScore(value = 0) {
    user.score += value
    userScore.innerHTML = user.score;
}

function addToBankScore(value = 0) {
    bank.score += value
    bankScore.innerHTML = bank.score;
}

function setBet(bet){
    if(bet < 0 || bet > AVAILABLE_BETS.length-1) return
    let betValue = AVAILABLE_BETS[bet]

    if(user.account < 0){
        bet = 0
        game.bet = bet
        gameBet.innerHTML = `${betValue}€`
    }

    if (betValue > user.account) return

    game.bet = bet
    gameBet.innerHTML = `${betValue}€`
}

function blackJack(who){
    if(who === 'user'){
        user.bj = true
        userBJ.innerHTML = "Black Jack !!"
        drawButton.classList.add('hidden')
    }

    if(who === 'bank'){
        bank.bj = true
        bankBJ.innerHTML = "Black Jack !!"
    }
}

function drawCard(who = "user", number = 1) {
    for (let i = 0; i < number; i++) {
        const index = Math.floor(Math.random() * cardGame.length)
        const card = cardGame[index];

        cardGame.splice(index, 1);

        if (who === "user") {
            user.cards.push(card);
            createHtmlCard(userCards, card)
            let value = card.value
            if(card.key === 'A'){
                value = bank.score + value > WIN_VALUE ? value - 10 : value
            }

            addToUserScore(value)
        }

        if (who === "bank") {
            bank.cards.push(card);
            createHtmlCard(bankCards, card)
            let value = card.value
            if(card.key === 'A'){
                value = bank.score + value > WIN_VALUE ? value - 10 : value
            }

            addToBankScore(value)
        }
    }
    if (who === "user") {
        checkUserScore()
    }

    if (who === "bank") {
        checkBankScore()
    }
}

function skip(){
    if(user.score < bank.score){
        checkUserScore(true)
    }else{
        drawCard('bank')
    }
}

function createHtmlCard(domElement, card) {
    const html = `
  <div class="card value-${card.value} ${card.name} relative" title="${card.key} of ${card.name}">
    <div class="color absolute top-0 left-1">
        <div class="small-container">
            <div>${card.key}</div>
            <img src="assets/${card.name}.png"  alt="${card.name} symbol"/>
        </div>
    </div>
    <div class="color absolute bottom-0 right-1 transform rotate-180">
        <div class="small-container">
            <div>${card.key}</div>
            <img src="assets/${card.name}.png"  alt="${card.name} symbol"/>
        </div>
    </div>
    <div class="blank-block"></div>
    <div class="w-full h-[70%] relative">
        ${cardHtmlDispatcher(card.key)(card.name)}
    </div>
    <div class="blank-block"></div>
  </div>
  `

    const cardElement = new DOMParser().parseFromString(html, "text/html").body.firstChild
    domElement.appendChild(cardElement)
}

function createCardGame() {
    numbers.forEach(({value, key}) => {
        cardGame.push(
            ...colors.map(({color, name, symbol}) => {
                return {
                    key,
                    value,
                    color,
                    symbol,
                    name,
                    id: `${name}-${key}`
                };
            })
        );
        cardGame.sort((a, b) => a.name.localeCompare(b.name));
    });
}

function resetField() {
    user.bj = false
    bank.bj = false

    bank.cards = []
    user.cards = []

    user.score = 0
    bank.score = 0

    addToBankScore(0)
    addToUserScore(0)

    userCards.innerHTML = ""
    bankCards.innerHTML = ""

    bankBJ.innerHTML = ""
    userBJ.innerHTML = ""

    gameState.innerHTML = ""

    drawButton.classList.remove('hidden')
    skipButton.classList.remove('hidden')
    resetButton.classList.add('hidden')

    incBet.classList.remove('hidden')
    decBet.classList.remove('hidden')

    game.betTaken = false


    setBet(0)
}

function checkUserScore(loose = false){
    if(user.cards.length > 2 || bank.cards.length > 2) {

        if(!game.betTaken){
            game.betTaken = true
            setUserAccount(user.account - AVAILABLE_BETS[game.bet])
        }

        if (!incBet.classList.contains('hidden')) {
            incBet.classList.add('hidden')

        }

        if (!decBet.classList.contains('hidden')) {
            decBet.classList.add('hidden')

        }
    }

    if(user.score === WIN_VALUE){
        drawButton.classList.add('hidden')
    }

    if (user.score > WIN_VALUE || (bank.score === WIN_VALUE && user.score !== WIN_VALUE) || loose){

        resetButton.classList.remove('hidden')
        drawButton.classList.add('hidden')
        skipButton.classList.add('hidden')

        gameState.innerHTML = "Défaite..."
        setUserAccount(user.account - AVAILABLE_BETS[game.bet])

        return
    }
}


function checkBankScore(){
    if (bank.score > WIN_VALUE){

        resetButton.classList.remove('hidden')
        drawButton.classList.add('hidden')
        skipButton.classList.add('hidden')

        gameState.innerHTML = "Victoire !"
        setUserAccount(user.account + AVAILABLE_BETS[game.bet])

        if(user.bj || bank.bj){
            setUserAccount(user.account + (AVAILABLE_BETS[game.bet]/2))
        }

        return
    }

    if (bank.score === WIN_VALUE && user.score === WIN_VALUE){
        resetButton.classList.remove('hidden')
        drawButton.classList.add('hidden')
        skipButton.classList.add('hidden')

        gameState.innerHTML = "Égalité"
        setUserAccount(user.account + AVAILABLE_BETS[game.bet])

        if(user.bj || bank.bj){
            setUserAccount(user.account + (AVAILABLE_BETS[game.bet]/2))
        }

        return
    }

}

function init(withAccount = false) {
    resetField();
    createCardGame();

    if(withAccount){
        setUserAccount(100)
    }

    drawCard("bank", 2);

    if(bank.score === WIN_VALUE){
        blackJack('bank')
    }
    drawCard("user", 2);
    if(user.score === WIN_VALUE){
        blackJack('user')
    }
}

init(true);
