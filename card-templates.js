
// Cards

const head = (key) => (color) => `
 <img class="absolute inset-0 m-auto head-img" src="assets/${color}.png"  alt="${color} symbol"/>
 <div class="absolute inset-0 m-auto w-min h-min text-white head uppercase text-2xl">${key}</div>
`

const two = (color) => `
 <img class="absolute top-0 left-0 right-0 m-auto color-symbol" src="assets/${color}.png"  alt="${color} symbol"/>
 <img class="absolute bottom-0 left-0 right-0 m-auto color-symbol transform rotate-180" src="assets/${color}.png"  alt="${color} symbol"/>
`

const three = (color) => `
  <img class="absolute top-0 left-0 right-0 m-auto color-symbol" src="assets/${color}.png"  alt="${color} symbol"/>
  <img class="absolute inset-0 m-auto color-symbol" src="assets/${color}.png"  alt="${color} symbol"/>
  <img class="absolute bottom-0 left-0 right-0 m-auto color-symbol transform rotate-180" src="assets/${color}.png"  alt="${color} symbol"/>
`

const four = (color) => `
  <img class="absolute top-0 left-5 color-symbol" src="assets/${color}.png"  alt="${color} symbol"/>
  <img class="absolute top-0 right-5 color-symbol" src="assets/${color}.png"  alt="${color} symbol"/>
  <img class="absolute bottom-0 left-5 color-symbol transform rotate-180" src="assets/${color}.png"  alt="${color} symbol"/>
  <img class="absolute bottom-0 right-5 color-symbol transform rotate-180" src="assets/${color}.png"  alt="${color} symbol"/>
`

const five = (color) => `
 <img class="absolute inset-0 m-auto color-symbol" src="assets/${color}.png"  alt="${color} symbol"/>
 <img class="absolute top-0 left-5 color-symbol" src="assets/${color}.png"  alt="${color} symbol"/>
 <img class="absolute top-0 right-5 color-symbol" src="assets/${color}.png"  alt="${color} symbol"/>
 <img class="absolute bottom-0 left-5 color-symbol transform rotate-180" src="assets/${color}.png"  alt="${color} symbol"/>
 <img class="absolute bottom-0 right-5 color-symbol transform rotate-180" src="assets/${color}.png"  alt="${color} symbol"/>
`

const six = (color) => `
  <img class="absolute top-0 left-5 right-0 color-symbol" src="assets/${color}.png"  alt="${color} symbol"/>
  <img class="absolute top-0 right-5 color-symbol" src="assets/${color}.png"  alt="${color} symbol"/>
  <img class="absolute top-0 bottom-0 left-5 m-auto color-symbol" src="assets/${color}.png"  alt="${color} symbol"/>
  <img class="absolute top-0 bottom-0 right-5 m-auto color-symbol" src="assets/${color}.png"  alt="${color} symbol"/>
  <img class="absolute bottom-0 left-5 color-symbol transform rotate-180" src="assets/${color}.png"  alt="${color} symbol"/>
  <img class="absolute bottom-0 right-5 color-symbol transform rotate-180" src="assets/${color}.png"  alt="${color} symbol"/>
`

const seven = (color) => `
  <img class="absolute top-0 left-5 right-0 color-symbol" src="assets/${color}.png"  alt="${color} symbol"/>
  <img class="absolute top-0 right-5 color-symbol" src="assets/${color}.png"  alt="${color} symbol"/>
  <img class="absolute top-8 left-0 right-0 m-auto color-symbol" src="assets/${color}.png"  alt="${color} symbol"/>
  <img class="absolute top-0 bottom-0 left-5 m-auto color-symbol" src="assets/${color}.png"  alt="${color} symbol"/>
  <img class="absolute top-0 bottom-0 right-5 m-auto color-symbol" src="assets/${color}.png"  alt="${color} symbol"/>
  <img class="absolute bottom-0 left-5 color-symbol transform rotate-180" src="assets/${color}.png"  alt="${color} symbol"/>
  <img class="absolute bottom-0 right-5 color-symbol transform rotate-180" src="assets/${color}.png"  alt="${color} symbol"/>
`

const eight = (color) => `
  <img class="absolute top-0 left-5 right-0 color-symbol" src="assets/${color}.png"  alt="${color} symbol"/>
  <img class="absolute top-0 right-5 color-symbol" src="assets/${color}.png"  alt="${color} symbol"/>
  <img class="absolute top-8 left-0 right-0 m-auto color-symbol" src="assets/${color}.png"  alt="${color} symbol"/>
  <img class="absolute top-0 bottom-0 left-5 m-auto color-symbol" src="assets/${color}.png"  alt="${color} symbol"/>
  <img class="absolute top-0 bottom-0 right-5 m-auto color-symbol" src="assets/${color}.png"  alt="${color} symbol"/>
  <img class="absolute bottom-8 left-0 right-0 m-auto color-symbol transform rotate-180" src="assets/${color}.png"  alt="${color} symbol"/>
  <img class="absolute bottom-0 left-5 color-symbol transform rotate-180" src="assets/${color}.png"  alt="${color} symbol"/>
  <img class="absolute bottom-0 right-5 color-symbol transform rotate-180" src="assets/${color}.png"  alt="${color} symbol"/>
`

const nine = (color) => `
  <img class="absolute top-0 left-5 color-symbol" src="assets/${color}.png"  alt="${color} symbol"/>
  <img class="absolute top-0 right-5 color-symbol" src="assets/${color}.png"  alt="${color} symbol"/>
  <img class="absolute top-10 left-5 color-symbol" src="assets/${color}.png"  alt="${color} symbol"/>
  <img class="absolute top-10 right-5 color-symbol" src="assets/${color}.png"  alt="${color} symbol"/>
  <img class="absolute inset-0 m-auto color-symbol" src="assets/${color}.png"  alt="${color} symbol"/>
  <img class="absolute bottom-10 left-5 color-symbol transform rotate-180" src="assets/${color}.png"  alt="${color} symbol"/>
  <img class="absolute bottom-10 right-5 color-symbol transform rotate-180" src="assets/${color}.png"  alt="${color} symbol"/>
  <img class="absolute bottom-0 left-5 color-symbol transform rotate-180" src="assets/${color}.png"  alt="${color} symbol"/>
  <img class="absolute bottom-0 right-5 color-symbol transform rotate-180" src="assets/${color}.png"  alt="${color} symbol"/>
`

const ten = (color) => `
  <img class="absolute top-0 left-5 color-symbol" src="assets/${color}.png"  alt="${color} symbol"/>
  <img class="absolute top-0 right-5 color-symbol" src="assets/${color}.png"  alt="${color} symbol"/>
  <img class="absolute top-5 left-0 right-0 m-auto color-symbol" src="assets/${color}.png"  alt="${color} symbol"/>
  <img class="absolute top-10 left-5 color-symbol" src="assets/${color}.png"  alt="${color} symbol"/>
  <img class="absolute top-10 right-5 color-symbol" src="assets/${color}.png"  alt="${color} symbol"/>
  <img class="absolute bottom-10 left-5 color-symbol transform rotate-180" src="assets/${color}.png"  alt="${color} symbol"/>
  <img class="absolute bottom-10 right-5 color-symbol transform rotate-180" src="assets/${color}.png"  alt="${color} symbol"/>
  <img class="absolute bottom-5 left-0 right-0 m-auto color-symbol transform rotate-180" src="assets/${color}.png"  alt="${color} symbol"/>
  <img class="absolute bottom-0 left-5 color-symbol transform rotate-180" src="assets/${color}.png"  alt="${color} symbol"/>
  <img class="absolute bottom-0 right-5 color-symbol transform rotate-180" src="assets/${color}.png"  alt="${color} symbol"/>
`

export const cardHtmlDispatcher = (key) => {
    return {
        2: two,
        3: three,
        4: four,
        5: five,
        6: six,
        7: seven,
        8: eight,
        9: nine,
        10: ten,
        'J': head("J"),
        'Q': head("Q"),
        'K': head("K"),
        'A': head('A'),
    }[key]
}
